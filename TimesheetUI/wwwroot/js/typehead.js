﻿$(document).ready(function () {

    const baseUrl = "http://localhost:5000/api/search/";
    const personnels = new Bloodhound({
        datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: baseUrl + 'personnel',
            transform: response => $.map(response, directorship => ({
                value: directorship.id,
                label: directorship.name
            })),
            prepare: function (query, settings) {
                settings.url = settings.url + "?query=" + query
                settings.headers = {
                    "Authorization": "Bearer " + Cookies.get('token')
                };
                return settings;
            },
        },
        limit: 5
    });

    const directorships = new Bloodhound({
        datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: baseUrl + 'directorship',
            transform: response => $.map(response, directorship => ({
                value: directorship.id,
                label: directorship.name
            })),
            prepare: function (query, settings) {
                settings.url = settings.url + "?query=" + query
                settings.headers = {
                    "Authorization": "Bearer " + Cookies.get('token')
                };
                return settings;
            },
        },
        limit: 5
    });

    $('.js-typeaheadPersonnel').typeahead(null, {
        display: 'label',
        source: personnels
    }).on('typeahead:select', function (obj,datum) {
        $('#filter_personnel').val(datum.value);
    }).on('typeahead:change', function (obj,datum) {
        if (datum == "") {
            $('#filter_personnel').val("");
        }
    });

    $('.js-typeaheadDirectorship').typeahead(null, {
        display: 'label',
        source: directorships
    }).on('typeahead:select', function (obj,datum) {
        $('#filter_directorship').val(datum.value);
    }).on('typeahead:change', function (obj,datum) {
        if (datum=="") {
            $('#filter_directorship').val("");
        }
    });


    
});




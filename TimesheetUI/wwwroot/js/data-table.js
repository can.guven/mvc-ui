﻿$(document).ready(function () {
    const baseUrlApi = "http://localhost:5000/api/";

    var getEventsUrl = baseUrlApi + "events";

    const token = Cookies.get('token');

    const parseJwt = (token) => {
        try {
            return JSON.parse(atob(token.split('.')[1]));
        } catch (e) {
            return null;
        }
    };

    var parsedJwt = parseJwt(token);
    var userId = parsedJwt['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier'];
    var userDirectorshipId = parsedJwt['DirectorshipId'];
    var useRole = parsedJwt['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];

    var datatableColumns = [
        { data: 'directorshipName' },
        { data: 'personnelName' },
        { data: 'category' },
        { data: 'customerName' },
        { data: 'title' },
        { data: 'completedDesc' },
        { data: 'startDate' },
        { data: 'endDate' }
    ];

    var datatableColumnDefs = [{
        targets: [6, 7], render: function (data) {
            var date = new Date(data);
            return date.toLocaleString("tr-TR");
        }
    }];


    if (useRole === "Admin") {
        getEventsUrl = getEventsUrl + "/by-directorship/" + userDirectorshipId;
        datatableColumns = [
            { data: 'personnelName' },
            { data: 'category' },
            { data: 'customerName' },
            { data: 'title' },
            { data: 'completedDesc' },
            { data: 'startDate' },
            { data: 'endDate' }
        ];
        datatableColumnDefs = [{
            targets: [5, 6], render: function (data) {
                var date = new Date(data);
                return date.toLocaleString("tr-TR");
            }
        }];
    }
    if (useRole == "User") {
        getEventsUrl = getEventsUrl + "/by-personnel/" + userId;
        datatableColumns = [
            { data: 'category' },
            { data: 'customerName' },
            { data: 'title' },
            { data: 'completedDesc' },
            { data: 'startDate' },
            { data: 'endDate' }
        ];
        datatableColumnDefs = [{
            targets: [4, 5], render: function (data) {
                var date = new Date(data);
                return date.toLocaleString("tr-TR");
            }
        }];
    }
    var eventsTable = $('#eventsTable').DataTable({
        ajax: {
            url: getEventsUrl,
            dataSrc: "",
            headers: {
                'Authorization': `Bearer ${token}`,
            },
        },
        columns: datatableColumns,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: 'Excel Export',
                className: 'btn btn-success'
            },
            {
                extend: 'pdf',
                text: 'PDF Export',
                className: 'btn btn-danger'
            },
            {
                extend: 'csv',
                text: 'CSV Export',
                className: 'btn btn-primary'
            }
        ],
        columnDefs: datatableColumnDefs
    });

    eventsTable.buttons().container()
        .insertBefore('#clear-filter');

    $('#filter-event').on('click', function () {
        var _endDate = $('#filter_endDate').val();
        var _startDate = $('#filter_startDate').val();
        if (_endDate) {
            var date = new Date(_endDate);
            date.setHours(26, 59);
            _endDate = date.toISOString();
        }
        if (_startDate) {
            var date = new Date(_startDate);
            date.setHours(03, 00);
            _startDate = date.toISOString();
        }
        var filterQuery = "event?";
        var filterInputs = {
            personnelID: $('#filter_personnel').val(),
            directorshipID: $('#filter_directorship').val(),
            category: $('#filter_category').val(),
            customerName: $('#filter_customerName').val(),
            endDate: _endDate,
            startDate: _startDate,
        }
        if (useRole === "Admin") {
            filterInputs.directorshipID = userDirectorshipId;
        }
        if (useRole == "User") {
            filterInputs.personnelID = userId;
            filterInputs.directorshipID = userDirectorshipId;
        }
        var filterInputsNotNull = {

        }
        Object.keys(filterInputs).forEach(function (key) {
            if ($.trim(filterInputs[key]) != "") {
                filterInputsNotNull[key] = filterInputs[key]
            }
        });
        filterQuery += $.param(filterInputsNotNull);
        console.log(filterQuery);
        eventsTable.ajax.url(baseUrlApi + "search/" + filterQuery).load();
        $('#filterModal').modal('hide');
    });

    $('#clear-filter').on('click', function () {
        $('#filter-form')[0].reset();
        eventsTable.ajax.url(getEventsUrl).load();
    });

});
﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimesheetUI.Models
{
    public class PersonnelViewModel
    {
        public int Id { get; set; }
        public int DirectorshipID { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
    }
}

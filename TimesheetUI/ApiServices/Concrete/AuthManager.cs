﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TimesheetUI.ApiServices.Interfaces;
using TimesheetUI.Models;

namespace TimesheetUI.ApiServices.Concrete
{
    public class AuthManager : IAuthService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly HttpClient _httpClient;
        public AuthManager(IHttpContextAccessor httpContextAccessor, HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpContextAccessor = httpContextAccessor;
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/");
        }

        public async Task<bool> Login(PersonnelLoginModel personnelLoginModel)
        {
            var jsonData = JsonConvert.SerializeObject(personnelLoginModel);
            var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
            var responseMessage = await _httpClient.PostAsync("Auth/Login", content);
            
            if (responseMessage.IsSuccessStatusCode)
            {
                
                var accessToken = JsonConvert.DeserializeObject<AccessToken>(await responseMessage.Content.ReadAsStringAsync());
                _httpContextAccessor.HttpContext.Response.Cookies.Append("token", accessToken.Token);
                _httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", accessToken.Token);
                var responsePersonnel = await _httpClient.GetAsync("personnels/by-username/" + personnelLoginModel.Username);
                var personnel = JsonConvert.DeserializeObject<PersonnelViewModel>(await responsePersonnel.Content.ReadAsStringAsync());
                var userClaims = new List<Claim> ()
                {   new Claim(ClaimTypes.Name,personnel.Username),
                    new Claim(ClaimTypes.GivenName, personnel.Name),
                    new Claim(ClaimTypes.Role,personnel.Role),
                    new Claim(ClaimTypes.NameIdentifier,personnel.Id.ToString()),
                    new Claim("DirectorshipId",personnel.DirectorshipID.ToString())
                };
                var userIdentity = new ClaimsIdentity(userClaims, "User");
                var userPrincipal = new ClaimsPrincipal(new[] { userIdentity });

                await _httpContextAccessor.HttpContext.SignInAsync(userPrincipal);
                return true;
            }
            return false;
        }
        public async Task<bool> Logout()
        {
            await _httpContextAccessor.HttpContext.SignOutAsync();
            return true;
        }
    }
}

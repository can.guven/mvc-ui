﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using TimesheetUI.ApiServices.Interfaces;
using TimesheetUI.Models;

namespace TimesheetUI.ApiServices.Concrete
{
    public class EventManager : IEventService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly HttpClient _httpClient;
        public EventManager(IHttpContextAccessor httpContextAccessor, HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpContextAccessor = httpContextAccessor;
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/");
            _httpClient.DefaultRequestHeaders.Authorization =
            new AuthenticationHeaderValue("Bearer", _httpContextAccessor.HttpContext.Request.Cookies["token"]);

        }

        public async Task<PersonnelViewModel> GetPersonnel()
        {
            var response = await _httpClient.GetAsync($"personnels/{_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier)}");
            var personnel = JsonConvert.DeserializeObject<PersonnelViewModel>(await response.Content.ReadAsStringAsync());
            return personnel;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimesheetUI.Models;

namespace TimesheetUI.ApiServices.Interfaces
{
    public interface IAuthService
    {
        Task<bool> Login(PersonnelLoginModel personnelLoginModel);
        Task<bool> Logout();
    }
}

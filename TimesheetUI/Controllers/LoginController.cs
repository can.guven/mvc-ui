﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TimesheetUI.ApiServices.Interfaces;
using TimesheetUI.Models;

namespace TimesheetUI.Controllers
{
    public class LoginController : Controller
    {
    private readonly IAuthService _authService;
    public LoginController(IAuthService authService)
    {
        _authService = authService;
    }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Index(PersonnelLoginModel model)
        {
            if(await _authService.Login(model))
            {
                return RedirectToAction("Index","Home",null);
            }
            return View();
        }
        public async Task<IActionResult> Logout()
        {
            if (await _authService.Logout())
            {
                return RedirectToAction("Index", "Login", null);
            }
            return RedirectToAction("Index","Home");
        }
    }
}
